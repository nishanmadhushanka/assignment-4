#include <stdio.h>

int main()
{
    int number, i,j;
    printf ("Enter up to the table number starting from 1:");
    scanf ("%d",&number);
    printf ("Multiplication table from 1 to %d\n",number);
    for(int i=1; i<=10; i++)
    {
        for(int j=1; j<=number; j++)
        {
            if (j<= number-1)
            printf ("%d*%d = %d,",j,i,i*j);
            else
            printf("%d*%d = %d,",j,i,i*j);

        }
        printf ("\n");
    }
    return 0;
}
